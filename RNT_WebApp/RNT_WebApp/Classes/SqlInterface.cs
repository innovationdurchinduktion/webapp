﻿using System.Collections.Generic;
using System.Configuration;
using RNT_WebApp.API.Classes;
using MySql.Data.MySqlClient;
using DateTime = System.DateTime;

namespace RNT_WebApp.Classes
{
    using System.Linq;
    using System.Threading.Tasks;

    public static class SqlInterface
    {
        private static MySqlConnection Connector { get; set; }
        private static bool IsActive { get; set; }
        private static bool _isInitialized;

        public static async Task<bool> InitAsync()
        {
            if (_isInitialized)
            {
                //check if database is reachable
                if (await ConnectAsync() == false) return false;

                await DisconnectAsync();
                return true;
            }

            Connector = new MySqlConnection(ConfigurationManager.ConnectionStrings["mysqlConnString"].ConnectionString);
            _isInitialized = true;

            //check if database is reachable
            if (await ConnectAsync() == false) return false;

            await DisconnectAsync();
            return true;
        }

        private static async Task<bool> ConnectAsync()
        {
            try
            {
                if (IsActive)
                {
                    return true;
                }

                await Connector.OpenAsync();
                IsActive = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static async Task<bool> DisconnectAsync()
        {
            try
            {
                if (IsActive == false)
                {
                    return true;
                }

                await Connector.CloseAsync();
                IsActive = false;
                return true;
            }
            catch
            {
                return false;
            }
        }


        #region Items

        public static async Task<Queue<Item>> GetItemsAsync()
        {
            try
            {
                await ConnectAsync();

                return MergeQueues(await GetAvailableItemsAsync(), await GetRentedItemsAsync());
            }
            finally
            {
                await DisconnectAsync();
            }
        }

        private static Queue<Item> MergeQueues(Queue<Item> firstQueue, Queue<Item> secondQueue)
        {
            var newQueue = new Queue<Item>();

            while (firstQueue.Count!=0)
            {
                newQueue.Enqueue(firstQueue.Dequeue());
            }
            while (secondQueue.Count != 0)
            {
                newQueue.Enqueue(secondQueue.Dequeue());
            }

            return newQueue;
        }


        //get items
        private static async Task<Queue<Item>> GetAvailableItemsAsync()
        {
            //only selects active items
            var items = new Queue<Item>();
            try
            {
                using (var cmd =
                    new MySqlCommand(
                        "SELECT iid, name, description, curator.lname, curator.fname, add_date FROM item JOIN curator ON curator.cid = addedBy_CID LEFT JOIN rent USING(iid) WHERE isActive = TRUE GROUP BY iid HAVING SUM(returned_date IS NULL XOR from_date IS NULL) = 0 ORDER BY IID",
                        Connector))
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        items.Enqueue(new Item
                        {
                            Iid = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            AddedRentedBy = $"{reader.GetString(3)} {reader.GetString(4)}",
                            AddDateTime = reader.GetDateTime(5),
                            IsActive = true,
                            IsAvailable = true
                        });
                    }
                }
            }
            catch
            {
                // ignored
            }
            return items;
        }

        private static async Task<Queue<Item>> GetRentedItemsAsync()
        {
            var items = new Queue<Item>();
            using (var cmd = new MySqlCommand("SELECT iid, name, description, due_date, fname, lname FROM item JOIN rent USING(iid) WHERE returned_date IS NULL AND isActive = TRUE ORDER BY IID", Connector))
            {
                try
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            items.Enqueue(new Item
                            {
                                Iid = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Description = reader.GetString(2),
                                DueDateTime = reader.GetDateTime(3),
                                AddedRentedBy = $"{reader.GetString(5)} {reader.GetString(4)}",
                                IsAvailable = false,
                                IsActive = true
                            });
                        }
                    }
                }
                catch
                {
                    // ignored
                }
                return items;
            }
        }

        #endregion


        //curators
        public static async Task<Queue<Curator>> GetCuratorsAsync()
        {
            var curators = new Queue<Curator>();
            try
            {
                if (await ConnectAsync() == false) return null;

                using (var cmd = new MySqlCommand("SELECT CID, fname, lname, email_address, password FROM curator ORDER BY CID", Connector))
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var newCurator = new Curator
                        {
                            CId = reader.GetInt32(0),
                            FName = reader.GetString(1),
                            LName = reader.GetString(2),
                            EmailAddress = reader.GetString(3),
                            Password = reader.GetString(4)
                        };
                        curators.Enqueue(newCurator);
                    }

                    await DisconnectAsync();

                    return curators;
                }
            }
            catch
            {
                return null;
            }
        }


        #region API
        //API Methods
        public static async Task<Queue<APIItem>> GetItemsAsync(Dictionary<string,string> pattern)
        {
            var itemsToReturn = new Queue<APIItem>();
            try
            {
                string cmdText;

                bool useAnd;
                var isAvailable = false;
                if (pattern.ContainsKey("available"))
                {
                    if (pattern["available"] == "t")
                    {
                        //select only available items
                        cmdText =
                            "SELECT Name, Description FROM item LEFT JOIN rent USING(IID) WHERE due_date < NOW() AND returned_date IS NOT NULL AND ";
                        useAnd = false;
                        isAvailable = true;
                    }
                    else
                    {
                        //select only unavailable items
                        cmdText =
                            "SELECT Name, Description, due_date FROM item JOIN rent USING(IID) WHERE due_date > NOW() AND returned_date IS NULL AND ";
                        useAnd = false;
                    }
                }
                else
                {
                    //select only available items
                    cmdText =
                        "SELECT Name, Description FROM item LEFT JOIN rent USING(IID) WHERE due_date < NOW() AND returned_date IS NOT NULL AND ";
                    useAnd = false;
                    isAvailable = true;
                }
                if (pattern.ContainsKey("name"))
                {
                    cmdText += $"Name LIKE '%{pattern["name"]}%' ";
                    useAnd = true;  
                }
                if (pattern.ContainsKey("description"))
                {
                    if(useAnd)
                        cmdText += $"AND Description LIKE '%{pattern["description"]}%' ";
                    else
                        cmdText += $"Description LIKE '%{pattern["description"]}%' ";
                }
                cmdText += "ORDER BY IID";

                if (await ConnectAsync() == false) return null;

                using (var cmd = new MySqlCommand(cmdText, Connector))
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var newItem = new APIItem
                        {
                            Name = reader.GetString(0),
                            Description = reader.GetString(1),
                            IsAvailable = isAvailable
                        };
                        itemsToReturn.Enqueue(newItem);
                    }    
                }
                await DisconnectAsync();

                return itemsToReturn;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<Queue<APICurator>> GetCuratorsAsync(Dictionary<string, string> pattern)
        {
            var curators = new Queue<APICurator>();
            try
            {
                var cmdText = "SELECT fname, lname, email_address FROM curator WHERE ";

                var useAnd = false;
                if (pattern.ContainsKey("fname"))
                {
                    useAnd = true;
                    cmdText += $"fname LIKE '%{pattern["fname"]}%'";
                }
                if (pattern.ContainsKey("lname"))
                {
                    if (useAnd)
                    {
                        cmdText += $" AND lname LIKE '%{pattern["lname"]}%'";
                    }
                    else
                    {
                        useAnd = true;
                        cmdText += $" lname LIKE '%{pattern["lname"]}%'";
                    }
                }
                if (pattern.ContainsKey("email_address"))
                {
                    if (useAnd)
                    {
                        cmdText += $" AND email_address LIKE '%{pattern["email_address"]}%'";
                    }
                    else
                    {
                        cmdText += $" email_address LIKE '%{pattern["email_address"]}%'";
                    }   
                }
                cmdText += " ORDER BY CID";


                if (await ConnectAsync() == false) return null;

                using (var cmd = new MySqlCommand(cmdText, Connector))
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var newCurator = new APICurator
                        {
                            FName = reader.GetString(0),
                            LName = reader.GetString(1),
                            EmailAddress = reader.GetString(2)
                        };
                        curators.Enqueue(newCurator);
                    }

                    await DisconnectAsync();

                    return curators;
                }
            }
            catch
            {
                return null;
            }
        }

        public static async Task<Queue<APIRent>> GetRentsAsync(Dictionary<string, string> pattern)
        {
            var rents = new Queue<APIRent>();
            try
            {
                var cmdText =
                    "SELECT curator.fname, curator.lname, curator.email_address, " +
                    "item.name, item.description, " +
                    "rent.fname, rent.lname, rent.email_address, rent.from_date, rent.due_date FROM rent " +
                    "JOIN item USING(IID) JOIN curator USING(CID) WHERE item.isactive IS TRUE AND rent.due_date > NOW() AND rent.returned_date IS NULL";

                if (pattern.ContainsKey("curator-fname"))
                {
                    cmdText += $" AND curator.fname LIKE '%{pattern["curator.fname"]}%'";
                }
                if (pattern.ContainsKey("curator-lname"))
                {
                    cmdText += $" AND curator.lname LIKE '%{pattern["curator.lname"]}%'";
                }
                if (pattern.ContainsKey("curator-email_address"))
                {
                    cmdText += $" AND curator.email_address LIKE '%{pattern["curator.email_address"]}%'";
                }
                if (pattern.ContainsKey("item-name"))
                {
                    cmdText += $" AND item.name LIKE '%{pattern["item.name"]}%'";
                }
                if (pattern.ContainsKey("item-description"))
                {
                    cmdText += $" AND item.description LIKE '%{pattern["item.description"]}%'";
                }
                if (pattern.ContainsKey("fname"))
                {
                    cmdText += $" AND rent.fname LIKE '%{pattern["fname"]}%'";
                }
                if (pattern.ContainsKey("lname"))
                {
                    cmdText += $" AND rent.lname LIKE '%{pattern["lname"]}%'";
                }
                if (pattern.ContainsKey("email_address"))
                {
                    cmdText += $" AND rent.email_address LIKE '%{pattern["email_address"]}%'";
                }
                if (pattern.ContainsKey("from_date"))
                {
                    cmdText += $" AND rent.from_date LIKE '%{pattern["from_date"]}%'";
                }
                if (pattern.ContainsKey("due_date"))
                {
                    cmdText += $" AND rent.due_date LIKE '%{pattern["due_date"]}%'";
                }
                cmdText += " ORDER BY RID";

                if (await ConnectAsync() == false) return null;

                using (var cmd = new MySqlCommand(cmdText, Connector))
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var rentCurator = new APICurator
                        {
                            FName = reader.GetString(0),
                            LName = reader.GetString(1),
                            EmailAddress = reader.GetString(2)
                        };
                        var rentItem = new APIItem
                        {
                            Name = reader.GetString(3),
                            Description = reader.GetString(4),
                            //IsAvailable = available != null && available.Value
                            IsAvailable = false
                        };
                        var newRent = new APIRent
                        {
                            Curator = rentCurator,
                            Item = rentItem,

                            FName = reader.GetString(5),
                            LName = reader.GetString(6),
                            EmailAddress = reader.GetString(7),
                            From = reader.GetString(8),
                            Due = reader.GetString(9)
                        };
                        
                        rents.Enqueue(newRent);
                    }

                    await DisconnectAsync();

                    return rents;
                }
            }
            catch
            {
                return null;
            }
        }

        #endregion
    }
}