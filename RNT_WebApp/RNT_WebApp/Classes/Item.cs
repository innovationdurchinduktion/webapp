﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RNT_WebApp.Classes
{
    public class Item
    {
        public int Iid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsAvailable { get; set; }
        public int AddCid { get; set; }
        public string AddedRentedBy { get; set; }
        public DateTime AddDateTime { get; set; }
        public DateTime DueDateTime { get; set; }
    }
}