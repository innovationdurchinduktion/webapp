﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RNT_WebApp.Classes
{
    public class Curator
    {
        public int CId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string EmailAddress { get; set; }
        //Passowrd for all curators: sysadmin1207!
        public string Password { get; set; }
    }
}