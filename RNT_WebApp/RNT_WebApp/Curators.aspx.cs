﻿using RNT_WebApp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RNT_WebApp
{
    public partial class Curators : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SqlInterface.InitAsync().Result)
            {
                if (Page.IsPostBack) return;

                if (Session["cdict"] == null)
                {
                    Session.Add("csearch", new Dictionary<string, string>());
                    Session.Add("cdict", true);
                }
                var dict = Session["csearch"] as Dictionary<string, string>;
                dict = dict ?? new Dictionary<string, string>();

                if (dict.Count > 0)
                {
                    BuildData(SearchCurators(), true, dict.Keys.ToArray());
                    BuildChips();
                    return;
                }
                
                BuildData(SqlInterface.GetCuratorsAsync().Result, false, null);
            }
            else
            {
                CreateDatabaseErrorMessage();
            }
        }

        private void BuildData(IReadOnlyCollection<Curator> curators, bool highlight, string[] toHighlight)
        {
            //Maybe do something, so large screens see the table (maybe javascript)
            BuildDataList(curators, highlight, toHighlight);
        }

        #region table

        private void BuildDataTable(IReadOnlyCollection<Curator> curators, bool highlight, string[] toHighlight)
        {
            var htmlTableCurators = new StringBuilder();
            if (curators != null && curators.Count > 0)
            {
                CreateTableStart(ref htmlTableCurators);

                foreach (var curatorToAdd in curators)
                {
                    AddRow(ref htmlTableCurators, curatorToAdd, highlight, toHighlight);
                }
                CreateTableEnd(ref htmlTableCurators);
            }
            else
            {
                CreateNoDataMessage(ref htmlTableCurators, "No curators found");
            }
            lt_cTable.Text = htmlTableCurators.ToString();
        }

        private static void CreateTableStart(ref StringBuilder htmlTableBuilder)
        {
            htmlTableBuilder.AppendLine("<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\">");
            htmlTableBuilder.AppendLine("<thead>");
            htmlTableBuilder.AppendLine("<tr>");

            htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthCName\">Name</th>");
            htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthCName\">Last Name</th>");
            htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthCEmail\">Email Address</th>");

            htmlTableBuilder.AppendLine("</tr>");
            htmlTableBuilder.AppendLine("</thead>");
            htmlTableBuilder.AppendLine("<tbody>");
        }

        private static void AddRow(ref StringBuilder htmlRowBuilder, Curator curatorToAdd, bool highlight, IEnumerable<string> toHighlight)
        {
            htmlRowBuilder.AppendLine("<tr>");
            var fname = curatorToAdd.FName;
            var lname = curatorToAdd.LName;
            var email = curatorToAdd.EmailAddress;

            if (highlight)
            {
                foreach (var pattern in toHighlight)
                {
                    if (Regex.IsMatch(fname, pattern))
                    {
                        fname = Regex.Replace(fname, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(lname, pattern))
                    {
                        lname = Regex.Replace(lname, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(email, pattern))
                    {
                        email = Regex.Replace(email, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                }
            }

            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{fname}</td>");
            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{lname}</td>");
            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{email}</td>");

            htmlRowBuilder.AppendLine("</tr>");
        }

        private static void CreateTableEnd(ref StringBuilder htmlTableBuilder)
        {
            htmlTableBuilder.AppendLine("</tbody>");
            htmlTableBuilder.AppendLine("</table>");
        }

        #endregion

        #region list

        private void BuildDataList(IReadOnlyCollection<Curator> curators, bool highlight, string[] toHighlight)
        {
            var htmlListCurators = new StringBuilder();
            if (curators != null && curators.Any())
            {
                CreateListStart(ref htmlListCurators);

                foreach (var curator in curators)
                {
                    AddListRow(ref htmlListCurators, curator, highlight, toHighlight);
                }

                CreateListEnd(ref htmlListCurators);
            }
            else
            {
                CreateNoDataMessage(ref htmlListCurators, "No curators found");
            }
            lt_cTable.Text = htmlListCurators.ToString();
        }

        private static void AddListRow(ref StringBuilder htmlRowBuilder, Curator curatorToAdd, bool highlight, IEnumerable<string> toHighlight)
        {
            var fname = curatorToAdd.FName;
            var lname = curatorToAdd.LName;
            var email = curatorToAdd.EmailAddress;

            if (highlight)
            {
                foreach (var pattern in toHighlight)
                {
                    if (Regex.IsMatch(fname, pattern))
                    {
                        fname = Regex.Replace(fname, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(lname, pattern))
                    {
                        lname = Regex.Replace(lname, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(email, pattern))
                    {
                        email = Regex.Replace(email, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                }
            }
            htmlRowBuilder.AppendLine("<li class=\"mdl-list__item mdl-list__item--two-line\">");
            htmlRowBuilder.AppendLine("<span class=\"mdl-list__item-primary-content\">");
            htmlRowBuilder.AppendLine("<i class=\"material-icons mdl-list__item-icon\">person</i>");
            htmlRowBuilder.AppendLine($"<u>{fname} {lname}</u>");
            htmlRowBuilder.AppendLine($"<span class=\"mdl-list__item-sub-title\">{email}</span>");
            htmlRowBuilder.AppendLine("</span>");
            htmlRowBuilder.AppendLine("<span class=\"mdl-list__item-secondary-content\">" +
                                      $"<a class=\"mdl-list__item-secondary-action\" href=\"mailto:{curatorToAdd.EmailAddress}?Subject=RNT-Verleihsystem\">" +
                                      "<i class=\"material-icons\">mail_outline</i>" +
                                      "</a>" +
                                      "</span>");
            htmlRowBuilder.AppendLine("</li>");
        }

        private static void CreateListStart(ref StringBuilder htmlListBuilder)
        {
            htmlListBuilder.AppendLine("<ul class=\"mdl-list\" style=\"margin: auto; width:100%;\">");
        }

        private static void CreateListEnd(ref StringBuilder htmlListBuilder)
        {
            htmlListBuilder.AppendLine("</ul>");
        }

        #endregion

        private static void CreateNoDataMessage(ref StringBuilder writeErrorBuilder, string message)
        {
            writeErrorBuilder.AppendLine("<div class=\"mdl-color-text--red cResponsiveErrorText\">");
            writeErrorBuilder.AppendLine($"<p class=\"cResponsiveErrorText\">{message}</p>");
            writeErrorBuilder.AppendLine("</div>");
        }

        private void CreateDatabaseErrorMessage()
        {
            var htmlText = new StringBuilder();

            htmlText.AppendLine("<div class=\"mdl-color-text--red cResponsiveErrorText\">");
            htmlText.AppendLine("<p class=\"cResponsiveErrorText\">Database Error</p>");
            htmlText.AppendLine("</div>");

            lt_cTable.Text = htmlText.ToString();
        }


        #region Search
        protected void Btn_searchClicked_OnClick(object sender, EventArgs e)
        {
            var pattern = searchField.Value;

            var dict = Session["csearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.ContainsKey(pattern) == false)
            {
                var chipBuilder = new StringBuilder();

                if (ChipReturnsData(pattern))
                {
                    chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable\">");
                    chipBuilder.AppendLine($"<span class=\"mdl-chip__text\">{pattern}</span>");
                    chipBuilder.AppendLine(
                        $"<button type=\"button\" class=\"mdl-chip__action\" id=\"{pattern}\">" +
                        "  <i class=\"material-icons\">cancel</i>" +
                        "</button>");
                    chipBuilder.AppendLine("</span>");
                }
                else
                {
                    chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable\">");
                    chipBuilder.AppendLine($"<span class=\"mdl-chip__text mdl-color-text--accent\">{pattern}</span>");
                    chipBuilder.AppendLine(
                        $"<button type=\"button\" class=\"mdl-chip__action\" id=\"{pattern}\">" +
                        "  <i class=\"material-icons\">cancel</i>" +
                        "</button>");
                    chipBuilder.AppendLine("</span>");
                }

                dict.Add(pattern, chipBuilder.ToString());

                BuildChips();

                BuildData(SearchCurators(), true, dict.Keys.ToArray());
            }
            Session["csearch"] = dict;
            searchField.Value = string.Empty;
        }

        private static bool ChipReturnsData(string pattern)
        {
            return SqlInterface.GetCuratorsAsync().Result.Any(item =>
                Regex.IsMatch(item.FName, pattern) ||
                Regex.IsMatch(item.LName, pattern) ||
                Regex.IsMatch(item.EmailAddress, pattern));
        }

        private Queue<Curator> SearchCurators()
        {
            var results = new Queue<Curator>();
            var dict = Session["csearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            foreach (var patternChip in dict)
            {
                foreach (var curator in SqlInterface.GetCuratorsAsync().Result.Where(item =>
                    Regex.IsMatch(item.FName, patternChip.Key) ||
                    Regex.IsMatch(item.LName, patternChip.Key) ||
                    Regex.IsMatch(item.EmailAddress, patternChip.Key)))
                {
                    if (results.Any(current => current.CId == curator.CId)) continue;

                    results.Enqueue(curator);
                }
            }
            return results;
        }

        protected void btn_patternChipDelete_OnClick(object sender, EventArgs e)
        {
            var toDelete = tb_delete.Value;
            var dict = Session["csearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.ContainsKey(toDelete))
            {
                dict.Remove(toDelete);

                BuildChips();

                //make data table with queries
                if(dict.Count > 0)
                {
                    BuildData(SearchCurators(), true, dict.Keys.ToArray());
                }
                else
                {
                    BuildData(SqlInterface.GetCuratorsAsync().Result, false, null);
                }
            }

            Session["csearch"] = dict;
            tb_delete.Value = string.Empty;
        }

        private void BuildChips()
        {
            lt_searchChips.Text = string.Empty;
            var dict = Session["csearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            foreach (var chip in dict)
            {
                lt_searchChips.Text += chip.Value;
            }
            if (dict.Count <= 1) return;


            var chipBuilder = new StringBuilder();
            chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable\" style=\"padding: 0 3px !important;\">");
            chipBuilder.AppendLine("<span class=\"mdl-chip__text\"></span>");
            chipBuilder.AppendLine(
                "<button type=\"button\" class=\"mdl-chip__action\" id=\"deleteAll\" style=\"margin: 0 0 0 0 !important;\">" +
                "  <i class=\"material-icons\">cancel</i>" +
                "</button>");
            chipBuilder.AppendLine("</span>");
            lt_searchChips.Text += chipBuilder.ToString();
        }

        protected void btn_deleteAllChips_OnClick(object sender, EventArgs e)
        {
            var dict = Session["csearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.Count <= 1) return;

            dict.Clear();
            lt_searchChips.Text = "";
            BuildData(SqlInterface.GetCuratorsAsync().Result, false, null);
            Session["csearch"] = dict;
        }

        #endregion
    }
}