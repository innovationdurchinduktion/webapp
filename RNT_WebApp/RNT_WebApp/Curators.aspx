﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Curators.aspx.cs" Inherits="RNT_WebApp.Curators" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RNT-Curators</title>

    <!--Material-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />

    <link rel="stylesheet" type="text/css" href="./css/HomeCurators.css" />
    <link rel="stylesheet" type="text/css" href="./css/main.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <!--asp Buttons to call events serverside-->
            <asp:Button runat="server" ID="btn_searchClicked" class="cNoDisplay" OnClick="Btn_searchClicked_OnClick" />
            <asp:Button runat="server" ID="btn_patternChipDelete" class="cNoDisplay" OnClick="btn_patternChipDelete_OnClick" />
            <asp:Button runat="server" ID="btn_deleteAllChips" class="cNoDisplay" OnClick="btn_deleteAllChips_OnClick" />
            <input type="text" runat="server" id="tb_delete" class="cNoDisplay" />

            <!--contact fab-->
            <a id="contact-button"  href="About.html#contact" class="mdl-button mdl-js-button mdl-button--fab mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast mdl-shadow--4dp">
                <i class="material-icons">mail</i>
            </a>
            <div class="mdl-tooltip mdl-tooltip--top" data-mdl-for="contact-button">Contact us</div>

            <!-- Simple header with fixed tabs. -->
            <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
                <header class="mdl-layout__header" id="headerBar">
                    <div class="mdl-layout__header-row">
                        <!-- Title -->
                        <span class="mdl-layout-title">RNT Verleihsystem</span>

                        <div class="mdl-layout-spacer"></div>

                        <nav class="mdl-navigation mdl-layout--large-screen-only">
                            <a class="mdl-navigation__link" href="Home.aspx">Home</a>
                            <a class="mdl-navigation__link" href="#">Curators</a>
                            <a class="mdl-navigation__link" href="Help.html">Help</a>
                            <a class="mdl-navigation__link" href="About.html">About</a>
                        </nav>
                    </div>
                </header>

                <div class="mdl-layout__drawer cDrawer" id="drawer">
                    <span class="mdl-layout-title mdl-color--primary mdl-color-text--white" id="drawer-top">RNT Verleihsystem</span>

                    <nav class="mdl-navigation">

                        <a class="mdl-navigation__link" href="Home.aspx">Home</a>
                        <a class="mdl-navigation__link mdl-color-text--primary" href="#">Curators</a>
                        <a class="mdl-navigation__link" href="Help.html">Help</a>
                        <a class="mdl-navigation__link" href="About.html">About</a>
                        <div class="drawerLine"></div>
                        <span class="mdl-navigation__link mdl-color-text--primary">Help about the ...</span>
                        <a class="mdl-navigation__link" style="display:none;" href="Help.html#api">API</a>
                        <a class="mdl-navigation__link" href="Help.html#terminal">Terminal</a>
                        <a class="mdl-navigation__link" href="Help.html#webinterface">Webinterface</a>

                    </nav>
                </div>

                <main class="mdl-layout__content">
                    <a id="top"></a>
                    <!--Content-->
                    <div class="mdl-grid cMainGrid">

                        <div class="mdl-cell mdl-cell--1-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                        <div class="mdl-color--white mdl-shadow--4dp mdl-cell mdl-cell--10-col mdl-cell--12-col-phone mdl-cell--12-col-tablet cMainCellCurators">

                            <!--Headline-->
                            <h3>Curators</h3>

                            <div class="cSearchMargin">

                                <!--Searchbar-->
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label">

                                    <label class="mdl-button mdl-js-button mdl-button--icon" for="searchField">

                                        <i class="material-icons">search</i>
                                    </label>
                                    <div class="mdl-textfield__expandable-holder">

                                        <input class="mdl-textfield__input" runat="server" type="text" id="searchField" />
                                        <label class="mdl-textfield__label" for="searchField">case sensitive search</label>
                                    </div>
                                </div>
                                <div class="cChipDiv">
                                    <div>
                                        <asp:Literal runat="server" ID="lt_searchChips"></asp:Literal>
                                    </div>
                                </div>
                            </div>


                            <div style="border-top: 1px solid #e0e0e0; margin-top: 14px;">

                                <asp:Literal runat="server" ID="lt_cTable"></asp:Literal>
                            </div>
                        </div>
                    </div>

                    <!--Footer-->
                    <footer class="mdl-mega-footer">

                        <div class="mdl-mega-footer__top-section">

                            <div class="mdl-mega-footer__left-section">

                                <div class="mdl-logo">RNT - Verleihsystem</div>
                            </div>
                            <div class="mdl-mega-footer--right-section">

                                <a class="mdl-typography--font-light" id="topLink" href="#top">Back to Top
                                    <i class="material-icons">expand_less</i>
                                </a>
                            </div>
                        </div>
                        <div class="mdl-mega-footer__middle-section">

                            <div class="mdl-mega-footer--drop-down-section">

                                <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked />
                                <h1 class="mdl-mega-footer--heading">Pages</h1>

                                <ul class="mdl-mega-footer__link-list">

                                    <li><a href="Home.aspx">Home</a></li>
                                    <li><a href="Curators.aspx">Curators</a></li>
                                    <li><a href="Help.html">Help</a></li>
                                    <li><a href="About.html">About</a></li>
                                </ul>
                            </div>
                            <div class="mdl-mega-footer--drop-down-section">

                                <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked />
                                <h1 class="mdl-mega-footer--heading">Help</h1>

                                <ul class="mdl-mega-footer__link-list">

                                    <li><a href="Help.html#terminal">Terminal</a></li>
                                    <li><a href="Help.html#webinterface">Webinterface</a></li>
                                    <li style="display:none;"><a href="Help.html#api">API</a></li>

                                </ul>
                            </div>
                            <div class="mdl-mega-footer--drop-down-section">

                                <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked />
                                <h1 class="mdl-mega-footer--heading">Info</h1>

                                <ul class="mdl-mega-footer__link-list">

                                    <li><a href="About.html#features">Products</a></li>
                                    <li><a href="About.html#team">Team</a></li>
                                    <li><a href="About.html#contact">Contact</a></li>

                                </ul>
                            </div>

                            <div class="mdl-mega-footer__right-section">
                                A project by Felix Jahn, Elias Gall and Daniel Rathkolb
                            </div>
                        </div>
                        <div class="mdl-mega-footer__bottom-section">

                            <div class="mdl-textfield--align-right">Website made by Daniel Rathkolb</div>
                        </div>
                    </footer>
                </main>
            </div>

            <!--Material-->
            <script defer="" src="https://code.getmdl.io/1.3.0/material.min.js"></script>
            <!--JQuery-->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

            <script type="text/javascript" src="./js/konami.js"></script>
            <script type="text/javascript" src="./js/main.js"></script>
            <script>

                $(document).ready(function () {

                    $('#totop').click(function () {
                        $('html, body').animate({
                            scrollTop: $('#top').offset().top
                        }), 6000
                    });
                });

            </script>
        </div>
    </form>
</body>
</html>
