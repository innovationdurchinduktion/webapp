﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RNT_WebApp.Classes;

namespace RNT_WebApp
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SqlInterface.InitAsync().Result)
            {
                if (Page.IsPostBack) return;

                if (Session["idict"] == null)
                {
                    Session.Add("isearch", new Dictionary<string, string>());
                    Session.Add("idict", true);
                }
                var dict = Session["isearch"] as Dictionary<string, string>;
                dict = dict ?? new Dictionary<string, string>();

                if (dict.Count > 0)
                {
                    BuildData(SearchItems(), true, dict.Keys.ToArray());
                    BuildChips();
                    return;
                }

                BuildData(SqlInterface.GetItemsAsync().Result, false, null);
            }
            else
            {
                CreateDatabaseErrorMessage();
            }
        }

        private void BuildData(IReadOnlyCollection<Item> items, bool highlight, string[] toHighlight)
        {
            //Maybe do something, so large screens see the table (maybe javascript)
            BuildDataList(items, highlight, toHighlight);
        }

        #region table
        //table
        private void BuildDataTable(IReadOnlyCollection<Item> items, bool highlight, string[] toHighlight)
        {
            if (items != null && items.Any())
            {
                var anyAvailable = false;
                var anyUnavailable = false;

                var htmlTableAvailable = new StringBuilder();
                var htmlTableNotAvailable = new StringBuilder();

                CreateTableStart(ref htmlTableAvailable, true);
                CreateTableStart(ref htmlTableNotAvailable, false);

                var toolTips = new Stack<KeyValuePair<string, Item>>();

                foreach (var item in items)
                {
                    if (item.IsAvailable)
                    {
                        anyAvailable = true;
                        var toolTipId = AddRow(ref htmlTableAvailable, item, highlight, toHighlight);

                        if (!string.IsNullOrEmpty(toolTipId))
                        {
                            toolTips.Push(new KeyValuePair<string, Item>(toolTipId, item));
                        }
                    }
                    else
                    {
                        anyUnavailable = true;
                        var toolTipId = AddRow(ref htmlTableNotAvailable, item, highlight, toHighlight);
                        if (!string.IsNullOrEmpty(toolTipId))
                        {
                            toolTips.Push(new KeyValuePair<string, Item>(toolTipId, item));
                        }
                    }
                }
                CreateTableEnd(ref htmlTableAvailable);
                CreateTableEnd(ref htmlTableNotAvailable);

                if (toolTips.Count > 0)
                {
                    foreach (var item in toolTips)
                    {
                        if (item.Value.IsAvailable)
                        {
                            CreateDescriptionToolTip(ref htmlTableAvailable, item.Key, item.Value.Description, highlight, toHighlight);
                        }
                        else
                        {
                            CreateDescriptionToolTip(ref htmlTableNotAvailable, item.Key, item.Value.Description, highlight, toHighlight);
                        }
                    }
                }

                if (anyAvailable)
                {
                    tbl_available.Text = htmlTableAvailable.ToString();
                }
                else
                {
                    htmlTableAvailable = new StringBuilder();
                    CreateNoDataMessage(ref htmlTableAvailable, "No items available");
                    tbl_available.Text = htmlTableAvailable.ToString();
                }
                if (anyUnavailable)
                {
                    tbl_rented.Text = htmlTableNotAvailable.ToString();
                }
                else
                {
                    htmlTableNotAvailable = new StringBuilder();
                    CreateNoDataMessage(ref htmlTableNotAvailable, "No items rented");
                    tbl_rented.Text = htmlTableNotAvailable.ToString();
                }
            }
            else
            {
                var errorBuilder = new StringBuilder();
                CreateNoDataMessage(ref errorBuilder, "No items found");
                tbl_available.Text = errorBuilder.ToString();
                tbl_rented.Text = errorBuilder.ToString();
            }
        }

        private static void CreateTableStart(ref StringBuilder htmlTableBuilder, bool isAvailable)
        {
            htmlTableBuilder.AppendLine(
                "<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--2dp\">");
            htmlTableBuilder.AppendLine("<thead>");
            htmlTableBuilder.AppendLine("<tr>");
            htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthName\">Name</th>");
            htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthDesc\">Description</th>");

            var addOrRent = isAvailable ? "Added By" : "Rented By";
            htmlTableBuilder.AppendLine($"<th class=\"mdl-data-table__cell--non-numeric cthAddRent\">{addOrRent}</th>");

            if (!isAvailable)
            {
                htmlTableBuilder.AppendLine("<th class=\"mdl-data-table__cell--non-numeric cthDueDate\">Due Date</th>");
            }

            htmlTableBuilder.AppendLine("</tr>");
            htmlTableBuilder.AppendLine("</thead>");
            htmlTableBuilder.AppendLine("<tbody>");
        }

        private static string AddRow(ref StringBuilder htmlRowBuilder, Item itemToAdd, bool highlight, IEnumerable<string> toHighlight)
        {
            htmlRowBuilder.AppendLine("<tr>");
            var name = itemToAdd.Name;
            var desc = itemToAdd.Description;
            var tooltipId = itemToAdd.Description;
            var addedRentedBy = itemToAdd.AddedRentedBy;
            var dueDate = itemToAdd.DueDateTime.ToString("dd.MM.yyyy hh:mm:ss");

            if (highlight)
            {
                foreach (var pattern in toHighlight)
                {
                    if (Regex.IsMatch(name, pattern))
                    {
                        name = Regex.Replace(name, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(desc, pattern))
                    {
                        desc = Regex.Replace(desc, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(addedRentedBy, pattern))
                    {
                        addedRentedBy = Regex.Replace(addedRentedBy, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                }
            }

            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{name}</td>");
            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\" id=\"{tooltipId}\">{desc}</td>");
            htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{addedRentedBy}</td>");

            if (!itemToAdd.IsAvailable)
            {
                //rented items extra column
                htmlRowBuilder.AppendLine($"<td class=\"mdl-data-table__cell--non-numeric\">{dueDate}</td>");
            }

            htmlRowBuilder.AppendLine("</tr>");
            return tooltipId;
        }

        private static void CreateTableEnd(ref StringBuilder htmlTableBuilder)
        {
            htmlTableBuilder.AppendLine("</tbody>");
            htmlTableBuilder.AppendLine("</table>");
        }
        
        #endregion

        #region list
        //list
        private void BuildDataList(IReadOnlyCollection<Item> items, bool highlight, string[] toHighlight)
        {
            if (items != null && items.Any())
            {
                var anyAvailable = false;
                var anyUnavailable = false;

                var htmlListAvailable = new StringBuilder();
                var htmlListUnavailable = new StringBuilder();

                CreateListStart(ref htmlListAvailable);
                CreateListStart(ref htmlListUnavailable);

                var toolTips = new Stack<KeyValuePair<string, Item>>();

                foreach (var item in items)
                {
                    if (item.IsAvailable)
                    {
                        anyAvailable = true;
                        var toolTipId = AddListRow(ref htmlListAvailable, item, highlight, toHighlight);

                        if (!string.IsNullOrEmpty(toolTipId))
                        {
                            toolTips.Push(new KeyValuePair<string, Item>(toolTipId, item));
                        }
                    }
                    else
                    {
                        anyUnavailable = true;
                        var toolTipId = AddListRow(ref htmlListUnavailable, item, highlight, toHighlight);

                        if (!string.IsNullOrEmpty(toolTipId))
                        {
                            toolTips.Push(new KeyValuePair<string, Item>(toolTipId, item));
                        }
                    }
                }

                CreateListEnd(ref htmlListAvailable);
                CreateListEnd(ref htmlListUnavailable);

                if (toolTips.Count > 0)
                {
                    foreach (var item in toolTips)
                    {
                        if (item.Value.IsAvailable)
                        {
                            CreateDescriptionToolTip(ref htmlListAvailable, item.Key, item.Value.Description, highlight,
                                toHighlight);
                        }
                        else
                        {
                            CreateDescriptionToolTip(ref htmlListUnavailable, item.Key, item.Value.Description, highlight,
                                toHighlight);
                        }
                    }
                }

                if (anyAvailable)
                {
                    tbl_available.Text = htmlListAvailable.ToString();
                }
                else
                {
                    if (highlight)
                    {
                        htmlListAvailable = new StringBuilder();
                        CreateNoDataMessage(ref htmlListAvailable, "No items found");
                        tbl_available.Text = htmlListAvailable.ToString();
                    }
                    htmlListAvailable = new StringBuilder();
                    CreateNoDataMessage(ref htmlListAvailable, "No items available");
                    tbl_available.Text = htmlListAvailable.ToString();
                }
                if (anyUnavailable)
                {
                    tbl_rented.Text = htmlListUnavailable.ToString();
                }
                else
                {
                    if (highlight)
                    {
                        htmlListUnavailable = new StringBuilder();
                        CreateNoDataMessage(ref htmlListUnavailable, "No items found");
                        tbl_rented.Text = htmlListAvailable.ToString();
                    }
                    htmlListUnavailable = new StringBuilder();
                    CreateNoDataMessage(ref htmlListUnavailable, "No items rented");
                    tbl_rented.Text = htmlListUnavailable.ToString();
                }
            }
            else
            {
                var errorBuilder = new StringBuilder();
                CreateNoDataMessage(ref errorBuilder, "No items found");
                tbl_available.Text = errorBuilder.ToString();
                tbl_rented.Text = errorBuilder.ToString();
            }
        }

        private static string AddListRow(ref StringBuilder htmlRowBuilder, Item itemToAdd, bool highlight, IEnumerable<string> toHighlight)
        {
            var name = itemToAdd.Name;
            var desc = itemToAdd.Description;
            var tooltip = itemToAdd.Description;
            var addedRentedBy = itemToAdd.AddedRentedBy;
            var dueDate = itemToAdd.DueDateTime.ToString("dd.MM.yyyy hh:mm:ss");

            if (highlight)
            {
                foreach (var pattern in toHighlight)
                {
                    if (Regex.IsMatch(name, pattern))
                    {
                        name = Regex.Replace(name, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(desc, pattern))
                    {
                        desc = Regex.Replace(desc, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                    if (Regex.IsMatch(addedRentedBy, pattern))
                    {
                        addedRentedBy = Regex.Replace(addedRentedBy, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                }
            }

            htmlRowBuilder.AppendLine("<li class=\"mdl-list__item mdl-list__item--three-line\">");
            htmlRowBuilder.AppendLine("<span class=\"mdl-list__item-primary-content\">");

            if (itemToAdd.IsAvailable)
            {
                htmlRowBuilder.AppendLine($"<span><u>{name}</u> added by: {addedRentedBy}</span>");
            }
            else
            {
                htmlRowBuilder.AppendLine(
                    $"<span><u>{name}</u> rented by: {addedRentedBy}. Due Date: {dueDate}</span>");
            }
            
            htmlRowBuilder.AppendLine($"<span class=\"mdl-list__item-text-body\" id=\"{tooltip}\">{desc}</span>");

            htmlRowBuilder.AppendLine("</span>");
            htmlRowBuilder.AppendLine("</li>");

            return tooltip;
        }

        private static void CreateListStart(ref StringBuilder htmlTableBuilder)
        {
            htmlTableBuilder.AppendLine("<ul class=\"mdl-list\">");
        }

        private static void CreateListEnd(ref StringBuilder htmlTableBuilder)
        {
            htmlTableBuilder.AppendLine("</ul>");
        }

        #endregion

        private static void CreateNoDataMessage(ref StringBuilder writeErrorBuilder, string message)
        {
            writeErrorBuilder.AppendLine("<div class=\"mdl-color-text--red cResponsiveErrorText\">");
            writeErrorBuilder.AppendLine($"<p class=\"cResponsiveErrorText\">{message}</p>");
            writeErrorBuilder.AppendLine("</div>");
        }

        private void CreateDatabaseErrorMessage()
        {
            var htmlText = new StringBuilder();

            htmlText.AppendLine("<div class=\"mdl-color-text--red cResponsiveErrorText\">");
            htmlText.AppendLine("<p class=\"cResponsiveErrorText\">Database Error</p>");
            htmlText.AppendLine("</div>");

            tbl_available.Text = htmlText.ToString();
            tbl_rented.Text = htmlText.ToString();
        }

        private static void CreateDescriptionToolTip(ref StringBuilder htmlBuilder, string id, string desc, bool highlight, IEnumerable<string> toHighlight)
        {
            if (highlight)
            {
                var words = desc;
                foreach (var pattern in toHighlight)
                {
                    if (Regex.IsMatch(words, pattern))
                    {
                        words = Regex.Replace(words, pattern,
                            $"<mark>{pattern}</mark>",
                            RegexOptions.IgnorePatternWhitespace);
                    }
                }
                htmlBuilder.AppendLine($"<div class=\"mdl-tooltip\" data-mdl-for=\"{id}\">{words}</div>");
                return;
            }
            htmlBuilder.AppendLine($"<div class=\"mdl-tooltip\" data-mdl-for=\"{id}\">{desc}</div>");
        }


        #region search

        protected void Btn_searchClicked_OnClick(object sender, EventArgs e)
        {
            //method is called, if the enter key is pressed in the searchbox
            //get searchpattern 

            //add deletable chip to Literal:lt_searchChips, which indicates the searchpattern
            //chip has to have an event, wich will be called, if it is removed

            //works
            var pattern = searchField.Value;

            var dict = Session["isearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.ContainsKey(pattern) == false)
            {
                var chipBuilder = new StringBuilder();

                if (ChipReturnsData(pattern))
                {
                    chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable\">");
                    chipBuilder.AppendLine($"<span class=\"mdl-chip__text\">{pattern}</span>");
                    chipBuilder.AppendLine(
                        $"<button type=\"button\" class=\"mdl-chip__action\" id=\"{pattern}\">" +
                        "  <i class=\"material-icons\">cancel</i>" +
                        "</button>");
                    chipBuilder.AppendLine("</span>");
                }
                else
                {
                    chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable\">");
                    chipBuilder.AppendLine($"<span class=\"mdl-chip__text mdl-color-text--accent\">{pattern}</span>");
                    chipBuilder.AppendLine(
                        $"<button type=\"button\" class=\"mdl-chip__action\" id=\"{pattern}\">" +
                        "  <i class=\"material-icons\">cancel</i>" +
                        "</button>");
                    chipBuilder.AppendLine("</span>");
                }
                dict.Add(pattern, chipBuilder.ToString());

                BuildChips();

                BuildData(SearchItems(), true, dict.Keys.ToArray());
            }
            Session["isearch"] = dict;
            searchField.Value = string.Empty;
        }

        private static bool ChipReturnsData(string pattern)
        {
            return SqlInterface.GetItemsAsync().Result.Any(item =>
                    Regex.IsMatch(item.Name, pattern) ||
                    Regex.IsMatch(item.Description, pattern) ||
                    Regex.IsMatch(item.AddedRentedBy, pattern));
        }

        private Queue<Item> SearchItems()
        {
            var items = SqlInterface.GetItemsAsync();
            var results = new Queue<Item>();

            var dict = Session["isearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            foreach (var patternChip in dict)
            {
                foreach (var item in items.Result.Where(item =>
                    Regex.IsMatch(item.Name, patternChip.Key) ||
                    Regex.IsMatch(item.Description, patternChip.Key) ||
                    Regex.IsMatch(item.AddedRentedBy, patternChip.Key)))
                {
                    if (results.Any(current => current.Iid == item.Iid)) continue;

                    results.Enqueue(item);
                }
            }
            return results;
        }

        protected void btn_patternChipDelete_OnClick(object sender, EventArgs e)
        {
            //deleting chip works
            var toDelete = tb_delete.Value;
            var dict = Session["isearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.ContainsKey(toDelete))
            {
                dict.Remove(toDelete);

                BuildChips();

                //make data tables with queries
                if (dict.Count > 0)
                {
                    BuildData(SearchItems(), true, dict.Keys.ToArray());
                }
                else
                {
                    BuildData(SqlInterface.GetItemsAsync().Result, false, null);
                }
            }

            Session["isearch"] = dict;
            tb_delete.Value = string.Empty;
        }

        private void BuildChips()
        {
            lt_searchChips.Text = string.Empty;
            var dict = Session["isearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            foreach (var chip in dict)
            {
                lt_searchChips.Text += chip.Value;
            }
            if (dict.Count <= 1) return;


            var chipBuilder = new StringBuilder();
            chipBuilder.AppendLine("<span class=\"mdl-chip mdl-chip--deletable cDeleteAllChip\">");
            chipBuilder.AppendLine("<span class=\"mdl-chip__text\"></span>");
            chipBuilder.AppendLine(
                "<button type=\"button\" class=\"mdl-chip__action\" id=\"deleteAll\">" +
                "  <i class=\"material-icons\">cancel</i>" +
                "</button>");
            chipBuilder.AppendLine("</span>");
            lt_searchChips.Text += chipBuilder.ToString();
        }

        protected void btn_deleteAllChips_OnClick(object sender, EventArgs e)
        {
            var dict = Session["isearch"] as Dictionary<string, string>;
            dict = dict ?? new Dictionary<string, string>();

            if (dict.Count <= 1) return;

            var items = SqlInterface.GetItemsAsync();

            dict.Clear();
            lt_searchChips.Text = "";

            BuildData(items.Result, false, null);
            Session["isearch"] = dict;
        }

        #endregion search

    }
}