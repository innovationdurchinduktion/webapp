﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RNT_WebApp.API;
using RNT_WebApp.Classes;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace RNT_WebApp.API
{
    public partial class API : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //url= .../API/API.aspx?name=daniel&desc=daniel
            //returned array with name=daniel und desc=daniel
            //can be accessed through = QueryString["name/daniel"]
            //TODO if the query has a space character it would be replaced by a + character
            var url = Request.QueryString;

            var line = url.ToString();

            if (string.IsNullOrEmpty(line) == false && Regex.IsMatch(line, "truncate", RegexOptions.IgnoreCase) == false)
            {
                var requestPairs = line.Split('&').Select(keyValue => keyValue.Split('=')).ToDictionary(items => items[0], items => items[1]);

                /*
                 * To understand the LinQ expression
                var requestPairs = new Dictionary<string,string>();

                foreach (var keyValue in line.Split('&'))
                {
                    var items = keyValue.Split('=');
                    requestPairs.Add(items[0], items[1]);
                }
                */
                if (requestPairs.Count > 0)
                {
                    switch (requestPairs["type"])
                    {
                        case "curator":
                            requestPairs.Remove("type");

                            if (SqlInterface.InitAsync().Result)
                            {
                                var itemsToReturn = SqlInterface.GetCuratorsAsync(requestPairs).Result.ToArray();
                                var json = JsonConvert.SerializeObject(itemsToReturn);

                                Response.Clear();
                                Response.ContentType = "application/json; charset=utf-8";
                                Response.Write(json);
                                Response.End();
                            }
                            
                            break;
                        case "item":
                            requestPairs.Remove("type");

                            if (SqlInterface.InitAsync().Result)
                            {
                                var itemsToReturnCurators = SqlInterface.GetItemsAsync(requestPairs).Result.ToArray();
                                var jsonCurators = JsonConvert.SerializeObject(itemsToReturnCurators);

                                Response.Clear();
                                Response.ContentType = "application/json; charset=utf-8";
                                Response.Write(jsonCurators);
                                Response.End();
                            }
                            
                            break;
                        case "rent":
                            //TODO debug query with itemname=test returns all rents
                            requestPairs.Remove("type");

                            if (SqlInterface.InitAsync().Result)
                            {
                                var itemsToReturnRent = SqlInterface.GetRentsAsync(requestPairs).Result.ToArray();
                                var jsonRent = JsonConvert.SerializeObject(itemsToReturnRent);

                                Response.Clear();
                                Response.ContentType = "application/json; charset=utf-8";
                                Response.Write(jsonRent);
                                Response.End();
                            }
                            
                            break;
                    }
                }
            }
        }
    }
}