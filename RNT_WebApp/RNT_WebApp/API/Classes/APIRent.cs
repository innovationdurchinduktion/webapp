﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RNT_WebApp.API.Classes
{
    public class APIRent
    {
        public APICurator Curator { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string EmailAddress { get; set; }
        public string From { get; set; }
        public string Due { get; set; }
        public APIItem Item { get; set; }
    }
}