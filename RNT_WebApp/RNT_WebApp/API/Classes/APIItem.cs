﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RNT_WebApp.API.Classes
{
    public class APIItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAvailable { get; set; }
    }
}