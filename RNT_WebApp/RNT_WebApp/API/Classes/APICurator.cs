﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RNT_WebApp.API.Classes
{
    public class APICurator
    {
        public string FName { get; set; }
        public string LName { get; set; }
        public string EmailAddress { get; set; }
    }
}