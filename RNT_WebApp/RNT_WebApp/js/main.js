﻿$(document).ready(function() {
    //btn_searchClicked//
    $('#searchField').keydown(function (e) {
        if (e.keyCode == 13) {
            $('#btn_searchClicked').click();
        }
    });

    //Delete chip clicked//
    $('.mdl-chip__action').click(function () {

        $('#tb_delete').val($(this).attr('id'));

        $('#btn_patternChipDelete').click();
    });

    //Delete all chip clicked//
    $('#deleteAll').click(function () {

        $('#btn_deleteAllChips').click();
    });

    //set bar height first time
    $('#drawer-top').height($('#headerBar').height());

    //set bar height on click
    $('div .mdl-layout__drawer-button').click(function () {

        $('#drawer-top').height($('#headerBar').height());
    });

    //catch resizing
    $(window).resize(function () {

        var heightofBar = $('#headerBar').height();
        if ($('#drawer').hasClass("is-visible")) {

            $('#drawer-top').height(heightofBar);
        } else {
            return;
        }
    });

});


