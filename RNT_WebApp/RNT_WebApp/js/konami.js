﻿$(document).ready(function() {
    // a key map of allowed keys
    var allowedKeys = {
        87: 'up',
        65: 'left',
        83: 'down',
        68: 'right',
        88: 'x',
        89: 'y',
        82: 'r',
        78: 'n',
        84: 't'
    };

    // the 'official' Konami Code sequence
    var konamiCode = ['r', 'n', 't', 'up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'x', 'y'];

    // a variable to remember the 'position' the user has reached so far.
    var konamiCodePosition = 0;

    // add keydown event listener
    document.addEventListener('keydown', function (e) {
        // get the value of the key code from the key map
        var key = allowedKeys[e.keyCode];
        // get the value of the required key from the konami code
        var requiredKey = konamiCode[konamiCodePosition];

        // compare the key with the required key
        if (key === requiredKey) {

            // move to the next key in the konami code sequence
            konamiCodePosition++;

            // if the last key is reached, activate cheats
            if (konamiCodePosition === konamiCode.length) {
                activate();
                konamiCodePosition = 0;
            }
        } else {
            konamiCodePosition = 0;
        }
    });

    function activate() {

        if ($('body').hasClass('rotate')) {
            $('body').removeClass('rotate');
        }
        $('body').addClass('rotate');

    }
});